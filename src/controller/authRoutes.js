const {registerUsers,findUser} = require('../models/modelslibrary'); //models library is just performing queries
const {forRegisterUser,forLoginUser}=require('../utils/validation');
const bcrypt = require('bcryptjs');
const JWT=require('jsonwebtoken');
require('dotenv').config()
async function registerUsersroutes(req, res, next){
    
        
        const values=Object.values(req.body)
        
         //encryption of password goes here
        const salt = await bcrypt.genSalt(10);
        const hashPassword = await bcrypt.hash(values[3], salt);
        values[3] = hashPassword;

        const result = forRegisterUser(req.body);//this is for validation
        
        if (result.error) {
          next({
            message: result.error.details[0].message,
            status: 500
          });
          return;
        }
        registerUsers(values).then(() => {
          res.send('You are succesfully registered');
        });
      
}
function findUserRoutes(req, res, next){
    
      
    const result = forRegisterUser(req.body);//this is for validation
    
    if (result.error) {
      next({
        message: result.error.details[0].message,
        status: 500
      });
      return;
    }
    
    
    findUser(req.body.emailid).then((catchedUser) => {

      if(catchedUser.rows.length){
        
        res.send("Sorry this email is already registered try using different email");
        return;  
      }
     else{
        next();
         }

    });
    
}
async function loginRoutes(req,res,next){

    //validating if the user provided details right or not
    const result = forLoginUser(req.body);//this is for validation
    
    if (result.error) {
      next({
        message: result.error.details[0].message,
        status: 500
      });
      return;
    }
    //checking email for login
    await findUser(req.body.emailid)
    .then((userDetails)=>{
        if(!userDetails.rows.length){
        res.send("Sorry Email or Password is wrong")
        return;
        }
        else{
           return userDetails.rows[0];            
        }
    })//checking password for login
    .then(async(user)=>{
         const validPassword=await bcrypt.compare(req.body.password,user.password)
        if(!validPassword)
          return res.status(400).send("Invalid Password");
    })
   //Do the work of token from here
    
   const token=JWT.sign({_id:req.body.emailid},process.env.TOKEN_SECRET);
   res.header('auth-token',token).send(token);

  next();
}

module.exports={
    registerUsersroutes,
    findUserRoutes,
    loginRoutes
   
}