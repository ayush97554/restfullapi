const { validationForPost,validationForUpdate,forGetAndDelete } = require('../utils/validation');
const {
  getBooksById,
  getBooks,
  postBooks,
  deleteBook,
  updateBook,
  
} = require('../models/modelslibrary'); //models library is just performing queries
function getBooksByIdRoutes(req, res, next) {
  const id = req.params.id;
  const result=forGetAndDelete(req.params);
  console.log("here")
  if(result.error){
    next(result.error.details[0]);
    return;
  }
  getBooksById(id).then(book => {
    if (book.rows.length) {
      res.send(book.rows);
    } else {
      next({
        message: 'Sorry the requested book is not found',
        status: 404
      });
    }
  });
}
function getBooksRoutes(req, res) {
  getBooks().then(books => res.send(books.rows));
}

function postBooksRoutes(req, res,next) {
  const values = Object.values(req.body);

  const result = validationForPost(req.body);
  
  if (result.error) {
    next({
      message: result.error.details[0].message,
      status: 500
    });
    return;
  }
  postBooks(values).then(() => {
    res.send('Sucessfully inserted the new entry');
  });
}

function deleteBookRoutes(req, res, next) {
  const id = req.params.id;
  const result=forGetAndDelete(req.params);
  if(result.error){
    next(result.error.details[0]);
    return;
  }
  deleteBook(id).then(deletedElement => {
    //If there is no such Id or element to delete
    if (deletedElement.rows.length) res.send(deletedElement.rows);
    else {
      next({
        details: 'There is no such Id to delete',
        status: 404
      });
    }
    //-------------------------------------------------------
  });
}
function updateBookRoutes(req, res, next) {
  const id = req.params.id;

  const values = Object.values(req.body);
  
  const result = validationForUpdate(req.body); //validation is done in seprate module
  
  if (result.error) {
    next({
      details: result.error.details[0].message,
      status: 500
    });
    return;
  }
  
  updateBook(id, values).then(updatedElement => {
    //Error is handled below--------------------------------------------------------
    if (updatedElement.rows.length) {
      //If the id does not exist
      res.send(updatedElement.rows);
    } else {
      next({
        details: 'There is no such Id to update',
        status: 500
      });
    }
    //----------------------------------------------------------------------------------------
  });
}

module.exports = {
  getBooksByIdRoutes,
  getBooksRoutes,
  postBooksRoutes,
  deleteBookRoutes,
  updateBookRoutes,
  
};
