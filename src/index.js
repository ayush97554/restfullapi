const express = require('express'); //return's function inside app module
const app = express();//this functions return objects which contains all the properties an express contains
const routelibrary = require('./routes/routelibrary');//index is starting point from there route->controller(library)->modelslibrary.js and reverse for getting data
const auth=require('./routes/auth.js');
const {errorHandler}=require('./middleware/errorHandling');
const {verify}=require('./utils/verify')
const logger = require('./middleware/logger');
const port = process.env.PORT || 3003;//if the port is any other than 3003 we will take that port  
app.listen(port, () => console.log(`Listening to port ${port}`)); //this line host my server to port variable
app.use(express.json());//express.json is a middleware which is used in request pipeline to convert the request to json
app.use(logger.log);
app.use('/api/user',auth);
app.use('/api',verify,routelibrary,errorHandler);//calling rootlibrary function inside routelibrary//the next function will pass the control to this block 
app.use('*', (req, res) => res.send('invalid url'));
app.use(logger.error);//winston is used to log error in a file so this is line is middleware from where the error is passed
  