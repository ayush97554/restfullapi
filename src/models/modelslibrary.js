const client1 = require('../config/pgClient');

async function getBooksById(idBooks) {
  try {
    console.log(idBooks);
    const query = `select * from library where id=$1;`;
    return await client1.query(query, [idBooks]);
  } catch (error) {
    return error;
  }
}
async function getBooks() {
  try {
    const query = 'select * from library;';
    return await client1.query(query);
  } catch (error) {
    return error;
  }
}
async function postBooks(data) {
  try {
    const query = 'insert into library values($1,$2,$3,$4,$5)';
    return await client1.query(query, data);
  } catch (error) {
    return error;
  }
}
async function deleteBook(Id) {
  try {
    const query = 'Delete from library where id=$1 RETURNING *';
    return await client1.query(query, [Id]);
  } catch (error) {
    return error;
  }
}
async function updateBook(id, values) {
  try {
    const query =
      'UPDATE library set Book=$1 ,Rating=$2 ,Genere=$3 ,Author=$4 where id=$5 RETURNING *';
    return await client1.query(query, [...values, id]);
  } catch (error) {
    return error;
  }
}
async function registerUsers(user) {
  try {
   
    
    const query = 'insert into users values($1,$2,$3,$4)';
    return await client1.query(query, user);
  } catch (error) {
    return error;
  }
}
async function findUser(emailid) {
  try {
    const query = 'select * from users where emailid=$1';
    return await client1.query(query, [emailid]);
  } catch (error) {
    return error;
  }
}

module.exports = {
  getBooksById,
  getBooks,
  postBooks,
  deleteBook,
  updateBook,
  registerUsers,
  findUser
};
