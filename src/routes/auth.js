const express = require('express');
const router = express.Router();

const {registerUsersroutes,findUserRoutes,loginRoutes}=require('../controller/authRoutes');

//registering goes here
router.post('/register',findUserRoutes,registerUsersroutes);//find user routes if not register user routes

//login of users goes here
router.post('/login',loginRoutes);

module.exports=router

