const express = require('express');
const router = express.Router();
const {
  getBooksByIdRoutes,
  getBooksRoutes,
  postBooksRoutes,
  deleteBookRoutes,
  updateBookRoutes
} = require('../controller/library');



router.get('/library', getBooksRoutes);
router.get('/library/:id', getBooksByIdRoutes);
router.post('/library', postBooksRoutes);
router.delete('/library/:id', deleteBookRoutes);
router.put('/library/:id', updateBookRoutes);
module.exports = router;
