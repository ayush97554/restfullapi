const path = require('path');

const client1 = require('./config/pgClient.js');

async function dropTable(client1) {
  await client1.query('DROP TABLE library');
  await client1.query('DROP TABLE author');
  await client1.query('DROP TABLE USERS');
}

async function convertCsvToTableForLibrary() {
  const pathForLibrary = await path.resolve('./', 'data/library.csv');

  await client1.query(
    `COPY library FROM '${pathForLibrary}' with DELIMITER ',' CSV HEADER`
  );
}
async function convertCsvToTableForAuthors() {
  const pathForAuthors = await path.resolve('./', 'data/author.csv');
  await client1.query(
    `COPY author FROM '${pathForAuthors}' with DELIMITER ',' CSV HEADER`
  );
}

async function creationOfTable() {
  try {
    await dropTable(client1).catch(err =>
      console.log(`There is error in dropping table if you are seeding first time igonore this error${err}`)
    );
    
    await client1.query(
      'CREATE TABLE IF NOT EXISTS  library(Id SERIAL,Book varchar(50),Rating INT,Genere varchar(50),Author varchar(50))'
    );
    await client1.query(
      'CREATE TABLE IF NOT EXISTS  users(Id SERIAL,name varchar(50),emailid varchar(50),password varchar(100))'
    );

    await convertCsvToTableForLibrary().catch(error =>
      console.log(`There is error in convert csv to table library ${error}`)
    );

    await client1.query(
      'CREATE TABLE IF NOT EXISTS author(Id INT,Name varchar(50),Country varchar(50))'
    );
    await convertCsvToTableForAuthors().catch(error =>
      console.log(`There is error in convert csv to table Authors ${error}`)
    );
    console.log('Creation of Database Successful');
    client1.end();
  } catch (err) {
    console.log('There is an error in query');
    console.log(err);
  }
}
creationOfTable();
