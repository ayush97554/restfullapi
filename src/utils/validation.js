const Joi = require('joi'); //joi is used for validation of inputs
function validationForPost(request) {
  const schema= {
    id: Joi.number().required(),
    book: Joi.string()
      .max(50)
      .alphanum()
      .required(),
    rating: Joi.number().required(),
    genere: Joi.string()
      .max(50)
      .required(),
    author: Joi.string()
      .max(50)
      .required()
  };
  
  
  return Joi.validate(request, schema);
}
function validationForUpdate(request) {
  const schema= {
    id: Joi.number(),
    book: Joi.string()
      .max(50)
      .alphanum()
      .required(),
    rating: Joi.number().required(),
    genere: Joi.string()
      .max(50)
      .required(),
    author: Joi.string()
      .max(50)
      .required()
  };
  
  
  return Joi.validate(request, schema);
}
function forGetAndDelete(request){
  const schema={
  id:Joi.number().integer().required()
  }
  return Joi.validate(request,schema);
}
function forRegisterUser(request){
  const schema={
  id:Joi.number().integer().required(),
  name:Joi.string().alphanum().required(),
  emailid:Joi.string().email().required(),
  password:Joi.string().alphanum().min(5).max(50).required()

  }
  return Joi.validate(request,schema);
}
function forLoginUser(request){
  const schema={
    emailid:Joi.string().email().required(),
    password:Joi.string().alphanum().min(5).max(50).required()
   }
    return Joi.validate(request,schema);
}
module.exports = {
  validationForPost,
  validationForUpdate,
  forGetAndDelete,
  forRegisterUser,
  forLoginUser
};
